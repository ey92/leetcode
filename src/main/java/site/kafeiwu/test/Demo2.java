package site.kafeiwu.test;

import com.sun.deploy.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Demo2 {
    public static final String METHOD_POST       = "POST";
    public static final String METHOD_GET        = "GET";
    public static final String CONTENT_TYPE_TEXT = "text/xml";
    public static final String CONTENT_TYPE_JSON = "application/json";

    public static void main(String[] args) throws Exception {
        String resultStr = invokeByType(METHOD_POST, "{\"applyId\":\"WD211404224998\",\"requireServiceDesc\":\"退款\",\"crmBusinessId\":\"SGDH210218H30J07\",\"storageLocation\":\"WO1\"}", "http://10.133.23.133:30002/sgdh/addSecondCallBySgdh", CONTENT_TYPE_JSON, 10000,10000);
        System.out.println(resultStr);
    }

    public static String invokeByType(String type, String content, String urlStr,
                                      String contentType, int connectTimeout,
                                      int readTimeout) throws Exception {
        URL url = new URL(urlStr);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        byte[] data = null;

        data = content.getBytes("utf-8");
//            httpConn.setRequestProperty("Content-Length", String.valueOf(data.length));

        httpConn.setRequestProperty("Content-Type", contentType + ";charset=utf-8");
        httpConn.setRequestMethod(type);
        httpConn.setConnectTimeout(connectTimeout);//连接超时
        httpConn.setReadTimeout(readTimeout);//读取数据超时
        httpConn.setDoOutput(true);
        if ("POST".equals(type) && null != data) {
            OutputStream out = null;
            try {
                httpConn.setDoInput(true);
                out = httpConn.getOutputStream();
                out.write(data);
                out.flush();
            } finally {
                if (null != out) {
                    out.close();
                }
            }
        }
        byte[] datas = readInputStream(httpConn.getInputStream());
        return new String(datas, "utf-8");
    }

    private static byte[] readInputStream(InputStream inStream) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = null;
        try {
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, len);
            }
        } catch (Exception e) {

        } finally {
            try {
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        data = outStream.toByteArray();//二进制数据
        return data;
    }
}



