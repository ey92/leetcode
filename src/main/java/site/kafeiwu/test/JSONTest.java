package site.kafeiwu.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class JSONTest {
    public static void main(String[] args) {
        JSONObject json = new JSONObject();
        json.put("a","111");
        json.put("b","222");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(json);
        System.out.println(jsonArray.toJSONString());
    }
}
