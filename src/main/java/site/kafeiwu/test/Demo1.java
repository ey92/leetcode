package site.kafeiwu.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Demo1 {
    public static void main(String[] args) {
        List<RefundResultData> list = new ArrayList<>();
        list.add(new RefundResultData("1111","1111"));
        list.add(new RefundResultData("2222", "2222"));
//        List<RefundResultData> resultData = JSON.parseObject(JSONObject.toJSONString(list), List.class);
//        System.out.println(resultData.get(0).getcOrderSn());
        List<RefundResultData> resultList =  new Gson().fromJson(JSONObject.toJSONString(list), new TypeToken<List<RefundResultData>>(){}.getType());
        System.out.println(resultList.toString());

    }

    static class RefundResultData {
        private String cOrderSn;
        private String fundType;

        public RefundResultData(String cOrderSn, String fundType) {
            this.cOrderSn = cOrderSn;
            this.fundType = fundType;
        }

        public String getcOrderSn() {
            return cOrderSn;
        }

        public void setcOrderSn(String cOrderSn) {
            this.cOrderSn = cOrderSn;
        }

        public String getFundType() {
            return fundType;
        }

        public void setFundType(String fundType) {
            this.fundType = fundType;
        }

        @Override
        public String toString() {
            return "RefundResultData{" +
                    "cOrderSn='" + cOrderSn + '\'' +
                    ", fundType='" + fundType + '\'' +
                    '}';
        }
    }
}

