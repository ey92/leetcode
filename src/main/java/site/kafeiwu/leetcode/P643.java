package site.kafeiwu.leetcode;
//给定 n 个整数，找出平均数最大且长度为 k 的连续子数组，并输出该最大平均数。
//
//
//
// 示例：
//
//
//输入：[1,12,-5,-6,50,3], k = 4
//输出：12.75
//解释：最大平均数 (12-5-6+50)/4 = 51/4 = 12.75
//
//
//
//
// 提示：
//
//
// 1 <= k <= n <= 30,000。
// 所给数据范围 [-10,000，10,000]。
//
// Related Topics 数组
public class P643 {

    public static void main(String[] args) {
        P643 p = new P643();
        int[] array = {1,12,-5,-6,50,3};
        System.out.println(p.findMaxAverage(array, 4));
    }

    /**
     * 暴力破解：从前到后取数
     */
    public double findMaxAverage(int[] nums, int k) {
        double count = 0;
        for (int i = 0; i < k; i++) {
            count += nums[i];
        }
        double maxCount = count;
        for (int i=k; i<nums.length; i++) {
            count = count - nums[i-k] + nums[i];
            maxCount = Math.max(maxCount, count);
        }
        return maxCount/k;
    }
}
