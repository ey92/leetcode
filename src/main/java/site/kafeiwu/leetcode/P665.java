package site.kafeiwu.leetcode;
//给你一个长度为 n 的整数数组，请你判断在 最多 改变 1 个元素的情况下，该数组能否变成一个非递减数列。
//
// 我们是这样定义一个非递减数列的： 对于数组中所有的 i (0 <= i <= n-2)，总满足 nums[i] <= nums[i + 1]。
//
//
//
// 示例 1:
//
// 输入: nums = [4,2,3]
//输出: true
//解释: 你可以通过把第一个4变成1来使得它成为一个非递减数列。
//
//
// 示例 2:
//
// 输入: nums = [4,2,1]
//输出: false
//解释: 你不能在只改变一个元素的情况下将其变为非递减数列。
//
//
//
//
// 说明：
//
//
// 1 <= n <= 10 ^ 4
// - 10 ^ 5 <= nums[i] <= 10 ^ 5
//
// Related Topics 数组
public class P665 {
    public static void main(String[] args) {
        P665 p = new P665();
        int[] array = {4,2,3}; //true
//        int[] array = {3,4,2,3}; //false
//        int[] array = {-1,4,2,3}; //true
        System.out.println(p.checkPossibility(array));
    }

    /**
     * 从头开始遍历，保证 nums[i] <= nums[i+1]
     * 出现 nums[i] > nums[i+1] 怎标记 进行修改  需要判断 nums[i-1]的值  把值改小
     */
    public boolean checkPossibility(int[] nums) {
        if (nums.length == 1) {
            return true;
        }
        boolean can = false;
        if (nums[1] < nums[0]) {
            nums[1] = Math.min(nums[0], nums[1]);
            can = true;
        }
        for (int i = 2; i < nums.length; i++) {
            if (nums[i] < nums[i-1]) {
                if (can) {
                    return false;
                }
                if (nums[i] >= nums[i-2]) {
                    nums[i-1] = nums[i-2];
                } else {
                    nums[i] = nums[i-1];
                }
                can = true;
            }
        }
        return true;
    }
}
