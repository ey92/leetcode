package site.kafeiwu.leetcode;

import site.kafeiwu.leetcode.util.ArraysUtils;

public class P832 {
    public static void main(String[] args) {
        System.out.println(1^1);
        System.out.println(0^1);
        P832 p = new P832();
        int[][] a = {{1,1,0},{1,0,1},{0,0,0}};
        ArraysUtils.print(p.flipAndInvertImage(a));
    }

    /**
     * 定义一个二维数组b，根据A反向取数，异或取反
     */
    public int[][] flipAndInvertImage(int[][] A) {
        int[][] b = new int[A.length][A[0].length];
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = A[i][A.length-j-1] ^ 1;
            }
        }
        return  b;
    }
    /**
     * 不使用其他数组，A反转位相比较  相等则说明需要取反，不相等则说明已经是取反的结果
     * left == right时，不能取反2次
     */
    public int[][] flipAndInvertImage2(int[][] A) {
        for (int i = 0; i < A.length; i++) {
            int left=0,right=A[i].length-left-1;
            for (; left < right; left++,right--) {
                if (A[i][left] == A[i][right]) {
                    A[i][left] ^= 1;
                    A[i][right] ^= 1;
                }
            }
            if (left == right) {
                A[i][left] ^=1;
            }
        }
        return A;
    }
}
