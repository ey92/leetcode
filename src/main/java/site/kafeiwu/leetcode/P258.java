package site.kafeiwu.leetcode;

//给定一个非负整数 num，反复将各个位上的数字相加，直到结果为一位数。
//
// 示例:
//
// 输入: 38
//输出: 2
//解释: 各位相加的过程为：3 + 8 = 11, 1 + 1 = 2。 由于2 是一位数，所以返回 2。
//
//
// 进阶:
//你可以不使用循环或者递归，且在 O(1) 时间复杂度内解决这个问题吗？
// Related Topics 数学
public class P258 {
    public static void main(String[] args) {
        P258 p = new P258();
        System.out.println(p.addDigits(38));
    }

    /**
     * 假设一个三位数整数 n=100*a+10*b+c,变化后 m=a+b+c；
     * 两者的差值 n-m=99a+9b，差值可以被9整除，说明每次缩小9的倍数
     * 那么我们可以对 res=num%9，若不为0则返回res，为0则返回9，输入为0除外
     */
    public int addDigits(int num) {
        if (num == 0) {
            return 0;
        } else {
            int i = num % 9;
            return i == 0 ? 9 : i;
        }
    }
}
