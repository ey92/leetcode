package site.kafeiwu.leetcode;

import site.kafeiwu.leetcode.entity.ListNode;

//将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
//
//
//
// 示例 1：
//
//
//输入：l1 = [1,2,4], l2 = [1,3,4]
//输出：[1,1,2,3,4,4]
//
//
// 示例 2：
//
//
//输入：l1 = [], l2 = []
//输出：[]
//
//
// 示例 3：
//
//
//输入：l1 = [], l2 = [0]
//输出：[0]
//
//
//
//
// 提示：
//
//
// 两个链表的节点数目范围是 [0, 50]
// -100 <= Node.val <= 100
// l1 和 l2 均按 非递减顺序 排列
//
// Related Topics 递归 链表
public class P21 {
    public static void main(String[] args) {
//        P21 p = new P21();
//        ListNode l1 = new ListNode(), l2 = new ListNode();
//        l1.add(1,2,4);
//        l2.add(1,3,4);
//        p.mergeTwoLists(l1, l2).printAll();
    }
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head = new ListNode();
        head.next = l1;
        ListNode pre=head;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                pre = l1;
                l1 = l1.next;
            } else {
                pre.next = l2;
                l2 = l2.next;
                pre = pre.next;
                pre.next = l1;
            }
        }
        if (l2 != null) {
            pre.next = l2;
        }
        return head.next;
    }
}
