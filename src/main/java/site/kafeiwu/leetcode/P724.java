package site.kafeiwu.leetcode;
//给定一个整数类型的数组 nums，请编写一个能够返回数组 “中心索引” 的方法。
//
// 我们是这样定义数组 中心索引 的：数组中心索引的左侧所有的元素相加和等于右侧所有元素相加的和。
//
// 如果数组不存在中心索引，那么我们应该返回 -1。如果数组有多个中心索引，那么我们应该返回最靠近左边的那一个。
//
//
//
// 示例 1：
//
// 输入：
//nums = [1, 7, 3, 6, 5, 6]
//输出：3
//解释：
//索引 3 (nums[3] = 6) 的左侧数之和 (1 + 7 + 3 = 11)，与右侧数之和 (5 + 6 = 11) 相等。
//同时, 3 也是第一个符合要求的中心索引。
//
//
// 示例 2：
//
// 输入：
//nums = [1, 2, 3]
//输出：-1
//解释：
//数组中不存在满足此条件的中心索引。
//
//
//
// 说明：
//
//
// nums 的长度范围为 [0, 10000]。
// 任何一个 nums[i] 将会是一个范围在 [-1000, 1000]的整数。
//
// Related Topics 数组
public class P724 {
    public static void main(String[] args) {
        P724 p = new P724();
        int[] nums = {1, 7, 3, 6, 5, 6};

//        System.out.println(p.pivotIndex1(nums));
        System.out.println(p.pivotIndex2(nums));
    }

    /**
     * 左边和 == 右边和
     * 中心点从左往后移动，左边和加，右边和减
     */
    public int pivotIndex1(int[] nums) {
        if (nums.length == 0) {
            return -1;
        }
        int left = 0, right = 0;
        for (int i = 1; i < nums.length; i++) {
            right += nums[i];
        }
        for (int i=0; i < nums.length;) {
            if (left == right) {
                return i;
            } else {
                left += nums[i];
                if (i+1 == nums.length){
                    return -1;
                } else {
                    right -= nums[++i];
                }
            }
        }
        return -1;
    }
    /**
     * 数组总值 = 2 * 左边和 + 中心数的值
     * 中心点从左往后移动，判断公式是否成立
     */
    public int pivotIndex2(int[] nums) {
        if (nums.length == 0) {
            return -1;
        }
        int total = 0;
        for (int i = 0; i < nums.length; i++) {
            total += nums[i];
        }
        int left = 0;
        for (int i = 0; i < nums.length; i++) {
            if (2*left + nums[i] == total) {
                return i;
            } else {
                left += nums[i];
            }
        }
        return -1;
    }
}
