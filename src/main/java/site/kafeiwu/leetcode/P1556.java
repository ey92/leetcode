package site.kafeiwu.leetcode;
//给你一个整数 n，请你每隔三位添加点（即 "." 符号）作为千位分隔符，并将结果以字符串格式返回。
//
//
//
// 示例 1：
//
// 输入：n = 987
//输出："987"
//
//
// 示例 2：
//
// 输入：n = 1234
//输出："1.234"
//
//
// 示例 3：
//
// 输入：n = 123456789
//输出："123.456.789"
//
//
// 示例 4：
//
// 输入：n = 0
//输出："0"
//
//
//
//
// 提示：
//
//
// 0 <= n < 2^31
//
// Related Topics 字符串
public class P1556 {
    public static void main(String[] args) {
        P1556 p = new P1556();
        System.out.println(p.thousandSeparator(50004));
    }

    /**
     * StringBuilder的方法 insert(int offset, String str)
     */
    public String thousandSeparator(int n) {
        StringBuilder sb = new StringBuilder().append(n);
        for (int i = sb.length()-3>0?sb.length()-3:0; i>0;) {
            sb.insert(i, ".");
            i = i-3>0?i-3:0;
        }
        return sb.toString();
    }
}
