package site.kafeiwu.leetcode;

import site.kafeiwu.leetcode.entity.ListNode;

//给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。
//
// 如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。
//
// 您可以假设除了数字 0 之外，这两个数都不会以 0 开头。
//
// 示例：
//
// 输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
//输出：7 -> 0 -> 8
//原因：342 + 465 = 807
//
// Related Topics 链表 数学
public class P2 {

    public static void main(String[] args) {
        P2 p = new P2();
        ListNode l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
        ListNode l2 = new ListNode(5, new ListNode(6, new ListNode(4)));

        p.addTwoNumbers(l1, l2).printAll();
    }

    /**
     * 逐位相加 满10往后一位加1
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0, l1);
        ListNode ln = head;
        for(;l2 != null || ln.next != null; ln = ln.next) {
            ln.next = ln.next == null ? new ListNode(0) : ln.next;
            if (l2 != null) {
                ln.next.val += l2.val;
                l2 = l2.next;
            }
            if (ln.next.val >= 10) {
                ln.next.val -= 10;
                if (ln.next.next == null) {
                    ln.next.next = new ListNode(1);
                } else {
                    ln.next.next.val++;
                }
            }
        }
        return head.next;
    }

}


