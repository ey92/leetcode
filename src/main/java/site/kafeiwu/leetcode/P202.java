package site.kafeiwu.leetcode;

import java.util.ArrayList;
import java.util.List;

//编写一个算法来判断一个数 n 是不是快乐数。
//
// 「快乐数」定义为：
//
//
// 对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和。
// 然后重复这个过程直到这个数变为 1，也可能是 无限循环 但始终变不到 1。
// 如果 可以变为 1，那么这个数就是快乐数。
//
//
// 如果 n 是快乐数就返回 true ；不是，则返回 false 。
//
//
//
// 示例 1：
//
//
//输入：19
//输出：true
//解释：
//12 + 92 = 82
//82 + 22 = 68
//62 + 82 = 100
//12 + 02 + 02 = 1
//
//
// 示例 2：
//
//
//输入：n = 2
//输出：false
//
//
//
//
// 提示：
//
//
// 1 <= n <= 231 - 1
//
// Related Topics 哈希表 数学
public class P202 {
    public static void main(String[] args) {
        P202 p = new P202();
        System.out.println(p.isHappy(19));
//        System.out.println(p.isHappy(2));
    }

    /**
     * 循环找到重复出现的数字则为 非快乐数
     */
    public boolean isHappy(int n) {
        List<Integer> list = new ArrayList<Integer>();
        int i = 0;
        while (n!=1) {
            int sum = 0;
            while (n > 0) {
                i = n % 10;
                n = n / 10;
                if (i == 0){
                    continue;
                }
                sum += i * i;
            }
            n = sum;
            if (list.contains(n)) {
                return false;
            }
            list.add(n);
            System.out.println(n);
        }
        return true;
    }
}
