package site.kafeiwu.leetcode;

import site.kafeiwu.leetcode.util.ArraysUtils;

//给你一个二维整数数组 matrix， 返回 matrix 的 转置矩阵 。
//
// 矩阵的 转置 是指将矩阵的主对角线翻转，交换矩阵的行索引与列索引。
//
//
//
//
//
// 示例 1：
//
//
//输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
//输出：[[1,4,7],[2,5,8],[3,6,9]]
//
//
// 示例 2：
//
//
//输入：matrix = [[1,2,3],[4,5,6]]
//输出：[[1,4],[2,5],[3,6]]
//
//
//
//
// 提示：
//
//
// m == matrix.length
// n == matrix[i].length
// 1 <= m, n <= 1000
// 1 <= m * n <= 105
// -109 <= matrix[i][j] <= 109
//
// Related Topics 数组
public class P867 {
    public static void main(String[] args) {
        P867 p = new P867();
        int[][] matrix = {{1,2,3},{4,5,6},{7,8,9}};
        ArraysUtils.print(p.transpose(matrix));
    }

    /**
     * 对角线反转 其实就是 行变列，列变行
     */
    public int[][] transpose(int[][] matrix) {
        int[][] array = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                array[i][j] = matrix[j][i];
            }
        }
        return array;
    }
}
