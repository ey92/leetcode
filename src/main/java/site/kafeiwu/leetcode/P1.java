package site.kafeiwu.leetcode;

import java.util.HashMap;
import java.util.Map;

//给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
//
//
//
// 示例:
//
// 给定 nums = [2, 7, 11, 15], target = 9
//
//因为 nums[0] + nums[1] = 2 + 7 = 9
//所以返回 [0, 1]
//
// Related Topics 数组 哈希表
public class P1 {

    public static void main(String[] args) {

    }

    /**
     * 双重循环
     */
    public int[] twoSum1(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target){
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{};
    }

    /**
     * 使用map key:与目标值的差值 value:坐标
     */
    public int[] twoSum2(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>(nums.length);
        for(int i = 0 ;i < nums.length; i++) {
            map.put(target-nums[i], i);
        }
        for(int i = 0 ;i < nums.length ; i++) {
            if(map.containsKey(nums[i]))// 差值包含在数组中
            {
                if(i == map.get(nums[i])) {// 排除像 nums = [3,2,4]，target=6这种特殊情况
                    continue;
                } else {
                    if(map.get(nums[i]) > i)
                    {
                        return new int[]{i, map.get(nums[i])};
                    } else {
                        return new int[]{map.get(nums[i]), i};
                    }
                }
            }
        }
        return new int[]{};
    }
}
