package site.kafeiwu.leetcode.entity;

public class ListNode {
    public int val;
    public ListNode next;
    public ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    /**
     * 以下方法为便于测试 自己定义
     * 禁止非测试使用
     */
    public ListNode(int... val) {
        this();
        ListNode ln = this;
        for (int i = 0; i < val.length; i++) {
            if (i == 0){
                this.val = val[i];
            } else {
                ln.next = new ListNode(val[i]);
                ln = ln.next;
            }
        }
    }

    public void printAll() {
        ListNode ln = this;
        StringBuilder sb = new StringBuilder();
        while(ln != null) {
            sb.append(ln.val).append("->");
            ln = ln.next;
        }
        System.out.println(sb.toString().substring(0, sb.length() - 2));
    }


    public static void main(String[] args) {
        ListNode listNode = new ListNode(1,2,3,4,5,6);
        listNode.printAll();
    }
}
